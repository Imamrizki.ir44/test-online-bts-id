<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shopping;

class ShoppingController extends Controller
{
    public function creteShopping(Request $request)
    {
        $shopping = Shopping::create([
            'Name' => $request->name,
        ]);
     
        return view('home',['shoppings' => $shopping]);
    }
}
