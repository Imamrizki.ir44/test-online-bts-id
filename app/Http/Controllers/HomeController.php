<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shopping;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $shopping = Shopping::get();
        return view('home',['shoppings' => $shopping]);
    }
}
