@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Shoping Name</label> 
                        </div>
                        <div class="col-md-6 text-right">
                            <label>Shoping Cart</label> 
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <form method="POST" action="#">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <input id="Name" type="text" class="form-control @error('Name') is-invalid @enderror" name="name" value="{{ old('Name') }}" required autocomplete="Name" autofocus placeholder="Shopping Name">
        
                                        @error('Name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-primary" disabled>
                                            {{ __('Submit') }}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <table class="table table-bordered">
                        <tr>
                            <th>Name</th>
                            <th>CreatedDate</th>
                        </tr>
                        @foreach($shoppings as $shopping)
                        <tr>
                            <td>{{ $shopping->Name }}</td>
                            <td>{{ $shopping->created_at }}</td>
                            <td>
                                <a href="/pegawai/edit/1">Edit</a>
                                |
                                <a href="/pegawai/hapus/2">Delete</a>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
